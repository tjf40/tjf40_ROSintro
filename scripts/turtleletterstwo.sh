#!/usr/bin/bash

rosservice call /reset

rosservice call /turtle1/teleport_absolute 3 6 0
rosservice call /clear

rosservice call /spawn 6 4 0 turtle2

rosservice call /turtle1/set_pen 200 100 0 3 0
rosservice call /turtle2/set_pen 0 255 75 3 0

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[1.0, -1.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-3, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[1.0, 1.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[1.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[0.0, -3, 0.0]' '[0.0, 0.0, 0.0]'

ostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist -- '[-3.0, -3.0, 0.0]' '[0.0, 0.0, 0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, -1.57079633]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'



