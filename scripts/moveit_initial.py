import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import time

try:
        from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
        from math import pi, fabs, cos, sqrt


class InitialDrawer:
    """ This class handles motion planning through moveit to draw the letters T, J and F """

    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("initial_drawer")  # Initialize ros node

        # Define and initialize moveit classes to interface with robot
        self.robot = moveit_commander.RobotCommander()
        self.scene = moveit_commander.PlanningSceneInterface()
        self.group_name = "manipulator"
        self.move_group = moveit_commander.MoveGroupCommander(self.group_name)

        # Initialize publisher to send messages to display_planned_path topic
        self.display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20,
        )

        self.scale = 1

        # Define robot variables from moveit
        self.planning_frame = self.move_group.get_planning_frame()
        self.eef_link = self.move_group.get_end_effector_link()
        self.group_names = self.robot.get_group_names()

        self.home_joint_goal = [0,-pi/2,0,0,0,0]  # Define home joint positions
        self.start_joint_goal = [-0.296, -0.76, 0.76, -pi, 0, pi]  # Define joint positions for where to start drawing the letters

    def move_home(self):
        """ Moves to the home joint state """
        self.go_to_joint_state(self.home_joint_goal)

    def go_to_joint_state(self, joint_goal):
        """ Moves the robot to the provided joint configuration

        :param joint_goal: List that contains the joint positions (in radians) for each of the 6 joints
        in order from 0 to 5.
        """
        assert len(joint_goal) == 6, "Joint goal should have a length of 6"
        self.move_group.go(joint_goal, wait=True)
        self.move_group.stop()

    def create_pose_goal(self, position, orientation=[0, 0, 0, 0]):
        """ Creates a Pose given a position and orientation vector

        :param position: List of length 3 to specify the x,y,z coordinates for the target pose
        :param orientation: List of length 4 to specify the x,y,z,w angles in radians for the target pose
        :return: geometry_msgs.msg.Pose for the target pose
        """
        assert len(position) == 3, "length of position should be 3"
        assert len(orientation) == 4, "length of orientation should be 4"
        pose_goal = geometry_msgs.msg.Pose()
        pose_goal.position.x = position[0]
        pose_goal.position.y = position[1]
        pose_goal.position.z = position[2]
        pose_goal.orientation.x = orientation[0]
        pose_goal.orientation.y = orientation[1]
        pose_goal.orientation.z = orientation[2]
        pose_goal.orientation.w = orientation[3]
        return pose_goal

    def go_to_pose_goal(self, pose_goal):
        """ Moves the robot to a target Pose

        :param pose_goal: geometry_msgs.msg.Pose for where the robot should be moved to
        """
        assert isinstance(pose_goal, geometry_msgs.msg.Pose), "pose_goal should be of type geometry_msgs.msg.Pose()"
        self.move_group.set_pose_target(pose_goal)
        success = self.move_group.go(wait=True)
        self.move_group.stop()
        self.move_group.clear_pose_targets()

    def move_to_relative_pose_goal(self, relative_pose):
        """ Moves the robot relative to current position by the values in the input Pose

        :param relative_pose: geometry_msgs.msg.Pose containing the relative movements to execute
        """
        pose_goal = self.move_group.get_current_pose().pose
        self.add_relative_pose_to_pose(pose_goal, relative_pose) 
        self.go_to_pose_goal(pose_goal) 

    def add_relative_pose_to_pose(self, pose, relative_pose):
        """ Adds the values of a relative pose to a pose

        Modifies the pose in place according to the values in the relative_pose and self.scale

        :param pose: geometry_msgs.msg.Pose to add the relative pose to
        :param relative_pose: geometry_msgs.msg.Pose with relative movements
        """
        pose.position.x += self.scale * relative_pose.position.x
        pose.position.y += self.scale * relative_pose.position.y
        pose.position.z += self.scale * relative_pose.position.z
        pose.orientation.x += relative_pose.orientation.x
        pose.orientation.y += relative_pose.orientation.y
        pose.orientation.z += relative_pose.orientation.z

    def plan_cartesian_path(self, relative_poses):
        """ Creates a plan for a cartesian path given a list of relative pose movements

        :param relative_poses: List[geometry_msgs.msg.Pose] of relative pose movements to execute
        :return: RobotTrajectory object for the movement plan
        """
        waypoints = self._get_waypoints_from_relative_movements(relative_poses)
        plan, fraction = self.compute_cartesian_path(waypoints)
        return plan

    def _get_waypoints_from_relative_movements(self, relative_poses):
        """ Get list of waypoints given a list of relative Pose movements

        :param relative_poses: List[geometry_msgs.msg.Pose] of relative pose movements
        :return: List[geometry_msgs.msg.Pose] of Pose waypoints along the desired path
        """
        waypoints = []
        wpose = self.move_group.get_current_pose().pose

        # For each relative pose, add it to the current pose and then append it to our list of waypoints
        for relative_pose in relative_poses:
            self.add_relative_pose_to_pose(wpose, relative_pose)
            waypoints.append(copy.deepcopy(wpose))

        return waypoints

    def compute_cartesian_path(self, waypoints):
        """ Compute the cartesian path given a list of Pose objects for where to go to

        :param waypoints: List[geometry_msgs.msg.Pose] of Pose waypoints along the desired path
        :return: Computed RobotTrajectory plan and fraction of how much of the path was followed
        """
        plan, fraction = self.move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
        return plan, fraction

    def execute_plan(self, plan):
        """ Executes a given RobotTrajectory

        :param plan: RobotTrajectory for the robot
        """
        self.move_group.execute(plan, wait=True)

    def print_robot_info(self):
        """ Prints information about the robot and its current state """
        planning_frame = self.move_group.get_planning_frame()
        print("============ Planning frame: %s" % planning_frame)

        eef_link = self.move_group.get_end_effector_link()
        print("============ End effector link: %s" % eef_link)

        group_names = self.robot.get_group_names()
        print("============ Available Planning Groups:", self.robot.get_group_names())
        print("============ Printing robot state")
        print(self.robot.get_current_state())
        print("")

    def sleep_and_print_start(self, letter):
        """ Sleeps and then prints that it is starting to draw the letter

        :param letter: Character that is about to be drawn
        """
        time.sleep(1)
        print(f"Starting to draw '{letter}'")

    def print_done_and_sleep(self, letter):
        """ Prints that is it done drawing the letter and then sleeps

        :param letter: Character that has just finished being drawn
        """
        print(f"Done drawing '{letter}'")
        time.sleep(1)

    def draw_t(self):
        """ Draws the letter 'T' with the end-effector """

        self.go_to_joint_state(self.start_joint_goal)  # Move to starting configuration

        relative_pose1 = self.create_pose_goal([-0.4, 0, 0])  # Move right 0.4 to draw the top of the T
        relative_pose2 = self.create_pose_goal([0.2, 0, 0])  # Move left 0.2 to get to the center of the T
        relative_pose3 = self.create_pose_goal([0, 0, -0.3])  # Move downwards to create base of the T

        plan = self.plan_cartesian_path([relative_pose1, relative_pose2, relative_pose3])

        # Execute plan, sleeping and printing info before and after
        self.sleep_and_print_start('T')
        self.execute_plan(plan)
        self.print_done_and_sleep('T')

    def draw_j(self):
        """ Draws the letter 'J' with the end-effector """

        self.go_to_joint_state(self.start_joint_goal)  # Move to starting configuration

        relative_pose1 = self.create_pose_goal([-0.4, 0, 0])  # Move right 0.4 to draw top of the J
        relative_pose2 = self.create_pose_goal([0.2, 0, 0])  # Move left 0.2 to get to center of the top of the J
        relative_pose3 = self.create_pose_goal([0, 0, -0.15])  # Move down
        relative_pose4 = self.create_pose_goal([0.02, 0, -0.06]) # Move down and to the left at steep slope
        relative_pose5 = self.create_pose_goal([0.04, 0, -0.06])  # Move down and to the left at medium slope
        relative_pose6 = self.create_pose_goal([0.04, 0, -0.03])  # Move down and to the left at shallow slope
        relative_pose7 = self.create_pose_goal([0.04, 0, 0.03])  # Move up and to the left at shallow slope
        relative_pose8 = self.create_pose_goal([0.04, 0, 0.06])  # Move up and to the left at medium slope
        relative_pose9 = self.create_pose_goal([0.02, 0, 0.06])  # Move up and to the left at steep slope

        plan = self.plan_cartesian_path([relative_pose1, relative_pose2, relative_pose3, relative_pose4, relative_pose5, relative_pose6, relative_pose7, relative_pose8])

        # Execute plan, sleeping and printing info before and after
        self.sleep_and_print_start('J')
        self.execute_plan(plan)
        self.print_done_and_sleep('J')

    def draw_f(self):
        """ Draws the letter 'F' with the end-effector """

        # Move to starting Pose
        self.go_to_joint_state(self.start_joint_goal)
        relative_pose0 = self.create_pose_goal([-0.2, 0, 0])  # Move right
        self.move_to_relative_pose_goal(relative_pose0)

        relative_pose1 = self.create_pose_goal([-0.3, 0, 0])  # Move right to create top horizontal line of the F
        relative_pose2 = self.create_pose_goal([0.3, 0, 0])  # Move left to get back to left side of the F
        relative_pose3 = self.create_pose_goal([0, 0, -0.15])  # Move down to middle of F
        relative_pose4 = self.create_pose_goal([-0.2, 0, 0])  # Move right to create bottom horizontal line of the F
        relative_pose5 = self.create_pose_goal([0.2, 0, 0])  # Move back to the left side of the F
        relative_pose6 = self.create_pose_goal([0, 0, -0.15])  # Move down to complete the F

        plan = self.plan_cartesian_path([relative_pose1, relative_pose2, relative_pose3, relative_pose4, relative_pose5, relative_pose6])

        # Execute plan, sleeping and printing info before and after
        self.sleep_and_print_start('F')
        self.execute_plan(plan)
        self.print_done_and_sleep('F')

if __name__ == "__main__":

    drawer = InitialDrawer()
    drawer.draw_t()
    drawer.draw_j()
    drawer.draw_f()
